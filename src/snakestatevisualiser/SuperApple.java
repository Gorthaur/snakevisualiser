package snakestatevisualiser;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

/**
 *
 * @author Jeremy
 */
public class SuperApple extends Apple {

    public SuperApple(String coordinates) {
        super(coordinates);
        getStyleClass().add("super-apple");
    }
}
