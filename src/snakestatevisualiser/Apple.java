/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package snakestatevisualiser;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;

/**
 *
 * @author Jeremy
 */
public class Apple extends Circle{
     public Apple(String coordinates)
     {
         String[] coordsSplit = coordinates.split("\\s++");
         double xCoord = Double.valueOf(coordsSplit[0]);
         double yCoord = Double.valueOf(coordsSplit[1]);
         
         setCenterX(xCoord *10 +5);
         setCenterY(yCoord *10 +5);
         setRadius(5.0);
         
         getStyleClass().add("apple");
     }
}
