package snakestatevisualiser;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Dean
 */
public class SnakeState {
    
    ArrayList<Snake> snakes = new ArrayList<Snake>();
    Shape snake1;
    Shape snake2;
    Shape snake3;
    Shape snake4;
    ArrayList<Apple> apples = new ArrayList<>();
    Shape apple1;
    Shape apple2;
    private String stateString;
    
    public SnakeState(String stateString) throws InvalidSnakeStateFormat {
        try {
            this.stateString = stateString;
            String[] lines = stateString.split("\n");

            apples.add(new SuperApple(lines[0]));
            apples.add(new Apple(lines[1]));

            String mySnakeNum = lines[2];

            snakes.add(new Snake(lines[3], 1));
            snakes.add(new Snake(lines[4], 2));
            snakes.add(new Snake(lines[5], 3));
            snakes.add(new Snake(lines[6], 4));

            snake1 = snakes.get(0).getSnakePolygon();
            snake2 = snakes.get(1).getSnakePolygon();
            snake3 = snakes.get(2).getSnakePolygon();
            snake4 = snakes.get(3).getSnakePolygon();
            
            apple1 = apples.get(0);
            apple2 = apples.get(1);

            if (snake1 != null) {
                snake1.getStyleClass().addAll("snake", "snake1");
            }

            if (snake2 != null) {
                snake2.getStyleClass().addAll("snake", "snake2");
            }

            if (snake3 != null) {
                snake3.getStyleClass().addAll("snake", "snake3");
            }

            if (snake4 != null) {
                snake4.getStyleClass().addAll("snake", "snake4");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new InvalidSnakeStateFormat();
        }
    }

    private void generateSampleSnakes() {
        Polygon n = new Polygon(new double[]{0, 0, 10, 0, 10, 500, 0, 500});
        Rectangle r = new Rectangle(0, 0, 100, 10);
        snake1 = Shape.union(r, n);
        snake1.getStyleClass().add("snake");
        snake1.getStyleClass().add("snake1");

        r = new Rectangle(49, 49, 100, 10);
        Rectangle r2 = new Rectangle(49, 59, (int)(Math.random()*200), 10);
        snake3 = Shape.union(r, r2);
        snake3.getStyleClass().add("snake");
        snake3.getStyleClass().add("snake3");
    }

    public String getStateString() {
        return stateString;
    }
}
